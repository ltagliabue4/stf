package linearRegression

import org.platanios.tensorflow.api.{Shape, Tensor}

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

object Utils {

  private[this] val random = new Random()

  def batch(batchSize: Int, weight: Float): (Tensor, Tensor) = {
    val inputs = ArrayBuffer.empty[Float]
    val outputs = ArrayBuffer.empty[Float]
    var i = 0
    while (i < batchSize) {
      val input = random.nextFloat()
      inputs += input
      outputs += weight * input
      i += 1
    }
    (Tensor(inputs).reshape(Shape(-1, 1)), Tensor(outputs).reshape(Shape(-1, 1)))
  }

}
