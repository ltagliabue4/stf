package linearRegression


import java.io._
import java.nio.file.Paths

import org.platanios.tensorflow.api.{Session, tf}
import org.tensorflow.framework.MetaGraphDef


object LinearRegression_restoreModel4Training {
/*  def main(args: Array[String]): Unit = {
      val metaPath = Paths.get("./model-store/my-model.meta")
      val metaGraphDef = MetaGraphDef.parseFrom(new BufferedInputStream(new FileInputStream(metaPath.toFile)))
      tf.Saver.fromMetaGraphDef(metaGraphDef)
  }*/


    def main(args: Array[String]): Unit = {
        val session = Session()
        var path = Paths.get("./model-store/my-model.meta")
        var checkpointPath = Paths.get("./model-store")
        var mgf = MetaGraphDef.parseFrom(new BufferedInputStream(new FileInputStream(path.toFile)))

        val saver = tf.Saver.fromMetaGraphDef(mgf)

        saver.restore(session, checkpointPath)
    }
}
